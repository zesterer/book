# Installing tools

To get started working on Veloren, you'll need to install a few things.

## Rust

Rust can be easily installed on all major desktop operating systems. Follow this link for installation instructions specific to your system.

<https://www.rust-lang.org/tools/install>

Because Veloren uses nightly version of Rust, please make sure you install it. You can find more details about nightly rust in [rustup repository](https://github.com/rust-lang/rustup.rs#working-with-nightly-rust). We also recommend to set up nightly only for Veloren directory, more details [here](https://github.com/rust-lang/rustup.rs#directory-overrides).

## Git

There are many ways to install Git. For those running Linux, your system probably has Git already installed or available to install with your chosen package manager.

For Windows, The ['Git for Windows'](https://gitforwindows.org/) suite is a sensible way to install Git, along with a set of tools that'll make it easier for you to use.

*For Mac OS, there exists other methods. I personally have not used Mac OS, so you'll likely have a better time finding instructions just by googling 'Install Git on Mac OS'. If someone wants to contribute appropriate instructions to this page, please do.*
