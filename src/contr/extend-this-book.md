# Contribute to this book

You can find the source for this book at our [gitlab](https://gitlab.com/veloren/book), feel free to make changes, correct errors, add experience.
