# This book is used by developers and users
# Yet we should sort it to make it easy accsessable for new users who just want to try it out, we don't want to scare them with to much contributing info
# Developers and power users know the book already and will find their section quite quickly after some time

- [Introduction](./intro/README.md)
  - [What is Veloren?](./intro/what.md)
  - [Who develops Veloren?](./intro/who.md)
- [Download](./dl/README.md)
  - [Compiling](./dl/compiling.md)
- [Contribute](./contr/README.md)
  - [Project Structure](./contr/project-structure.md)
  - [Toolchain](./contr/toolchain.md)
  - [Getting Started](./contr/getting-started.md)
  - [RFCs](./contr/rfcs.md)
  - [Generating Docs](./contr/generating-docs.md)
  - [Extend this Book](./contr/extend-this-book.md)
- [Dev Tricks](./dev/README.md)
  - [Git Workflow](./dev/workflow.md)
  - [Troubleshooting](./dev/troubleshooting.md)
- [Roadmap](./misc/roadmap.md)
